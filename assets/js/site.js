// Shane Schroll | Gardner Design Developer (https://gardnerdesign.com)
(function($) {
    $(document).ready(function() {

		let $body = $('body');

		// notification banner close function
		$('.close-notice').click(function() {
			$(this).parent().hide();
		});

		// show a loading symbol for filters once the dataset becomes larger
		// $(document).on('facetwp-refresh', function() {
        // 	$('.facetwp-template').prepend('<div class="is-loading"><span class="gif-loader"></span></div>');
		// });

		// // once a search is made, filtering turns into a "live filter"
		// $(document).on('facetwp-loaded', function() {
		// 	$('.facetwp-template .is-loading').remove();

		// 	// scroll to top of news list after selecting
		// 	if( $('#top').length > 0) {
		// 		var atop = $('#top').offset().top;
		// 		$('html')[0].scrollTo(0, atop - 90);
		// 	}

		// 	if(FWP.loaded) {
		// 		$('.facetwp-template').addClass('visible');
		// 		$('.facetwp-empty-loading').hide();
		// 	} else {
		// 		$('.facetwp-empty-loading').show();
		// 	}
		// });

		// accessible accordion block - controls and aria events for screenreaders
		$(function accordionBlock() {
			$('.accordion-content').each(function() {
				$(this).hide();
			});

			$('.accordion-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.toggleClass('remove-border');
					$this.removeClass('target');
					$this.attr('aria-pressed', 'true');
					$this.next('.accordion-content').slideToggle(350);
					$this.next('.accordion-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.accordion-content:first').slideToggle(350, function() {
						$this.prev('.accordion-block-title').addClass('target');
						$this.prev('.accordion-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
						$this.toggleClass('remove-border');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});
	}); // end Document.Ready
})(jQuery);