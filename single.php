<?php
/**
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// only setup for single posts
if( is_singular() ) :
	if( $post->post_parent != 0 ) {
		$context['is_child'] = true;
	} else {
		$context['is_parent'] = true;
	}
endif;

the_post();

Timber::render(['single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig'], $context);